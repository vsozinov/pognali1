﻿using System;

namespace pognali
{
    class CreditBroker
    {
        public bool IsCreditApproved(int age)
        {
            if (age < 25)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }

    class calculationPercent
    {
        public double percentCalc(double cash, int yersOfVklad)
        {
            for (int i = 0; i < yersOfVklad; i++)
            {
                var yearPercent = cash * 0.04;
                cash += yearPercent;
            }
            return cash;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var creditBroker = new CreditBroker();

            while (true)
            {
                Console.WriteLine("Введите свой возраст:");
                var ageString = Console.ReadLine();

                Console.WriteLine("Введите сумму вклада:");
                var cashUsers = Console.ReadLine();
                var cashUsersInt = int.Parse(cashUsers);
                var cashUsersWork = cashUsersInt * 1.0;

                Console.WriteLine("Введите срок вклада:");
                var years = Console.ReadLine();
                var yearsWork = int.Parse(years);

                var checkAge = int.TryParse(ageString, out int ageChecked);
                if (checkAge)
                {
                    var result = creditBroker.IsCreditApproved(ageChecked);
                    Console.WriteLine($"Результат одобрения: {result}");
                    Console.WriteLine();
                    if (result)
                    {
                        var capital = calculationPercent.percentCalc(cashUsersWork, yearsWork);
                    }
                } 
                else
                {
                    Console.WriteLine("kosyachok, vvedite age");
                    Console.WriteLine();
                }
                
            }
        }
    }
}
 